<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Nombre de usuario y/o contraseña incorrectos.',//'Estas credenciales no coinciden con nuestros registros.',
    'throttle' => 'Demasiados intentos de acceso. Por favor intente nuevamente en :seconds segundos.',
    'activacion' => 'Su cuenta está en proceso de revisión y activación, esto puede tardar un periodo máximo de 24 horas.',
    'confirmarcorreo' => 'Confirme su correo electrónico.',

];
