 <!--  MENU PARA TODO TIPO DE USUARIO-->
   <li {{ (Request::is("/") ? 'class=active' : '') }}>
      <a href="{{ URL::asset('/') }}"><i class="fa fa-home" aria-hidden="true"></i>
        <span class="nav-label">Inicio</span>
      </a>
  </li> 
   <li {{ (Request::is("numeros_aleatorios") ? 'class=active' : '') }}>    
      <a href="{{ URL::asset('numeros_aleatorios') }}">
        <span class="nav-label">Números Aleatorios</span>
      </a>
  </li>

     <li {{ (Request::is("linea-espera") ? 'class=active' : '') }}>    
      <a href="{{ URL::asset('linea-espera') }}">
        <span class="nav-label">Linea de Espera</span>
      </a>
  </li>

    <li {{ (Request::is("montecarlo") ? 'class=active' : '') }}>    
      <a href="{{ URL::asset('montecarlo') }}">
        <span class="nav-label">Montecarlo</span>
      </a>
  </li>

       <li {{ (Request::is("linea-espera-montecarlo") ? 'class=active' : '') }}>    
      <a href="{{ URL::asset('linea-espera-montecarlo') }}">
        <span class="nav-label">Linea de Espera Montecarlos</span>
      </a>
  </li>

         <li {{ (Request::is("inventario-montecarlo") ? 'class=active' : '') }}>    
      <a href="{{ URL::asset('inventario-montecarlo') }}">
        <span class="nav-label">Inventario Montecarlos</span>
      </a>
  </li>


   <li >
      <a href="{{ URL::asset('logout') }}"><i class="fa fa-sign-out" aria-hidden="true"></i>
        <span class="nav-label">Salir</span>
      </a>
  </li>