<!DOCTYPE html>
<html lang="es-EC" class="no-js" data-device-type="dedicated">
    <head>

<title>Plantilla | Acceso al Sistema</title> 

    {!! Html::style('admin/plantilla/css/bootstrap.min.css') !!}
    {!! Html::style('admin/plantilla/font-awesome/css/font-awesome.css') !!}
    {!! Html::style('admin/plantilla/css/animate.css') !!}
    {!! Html::style('admin/plantilla/css/style.css') !!}
 
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, minimal-ui, shrink-to-fit=no" />

 


<style id="antiClickjack">
    html.js body {display: none !important;}
</style>
 


{!! Html::style('admin/plantilla/css/login.css') !!}
    {!! Html::style('admin/plantilla/css/login2.css') !!}
    {!! Html::style('admin/plantilla/css/login3.css') !!}


</head>    
    <body class="row-fluid row hero-bg lightContent reverseLink holiday-hero">

        
        
<div id="body" class="">
<div role="main" id="main" >
    
        <div >

              
              <div class="middle-box text-center">

                <h1 class="logo-name"> 
                    {{ HTML::image('public/logomanta.png','LOGO',array('style'=>'width: 180px; ')) }} 
                    </h1>
                 <h3 class="contentPara" >Sistema Principal</h3>
                @include('alerts.errors')
                @include('alerts.request')
                @include('alerts.success')
                <p class="contentPara">Por favor ingrese sus Email y Contrasena para empezar.</p>
                <form action="login" method="post">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"> 

                    <div class="form-group" style="text-align: center;">
                        <input type="text" class="form-control" name="username" placeholder="Ingrese número de cédula" style="text-align: center;"  maxlength="10">
                        <i class="icon-user icon-large"></i>
                    </div>
                    

                    <div class="form-group" style="text-align: center;">
                        <input type="password" class="form-control" name="password"  placeholder="Ingrese su contraseña" style="text-align: center;">
                         <i class="icon-lock icon-large"></i>                           
                    </div>

                    <div class="form-group" style="text-align: center;">
                        <button type="submit" class="btn btn-sm btn-primary btn-block" style="    margin: 0px;background-color: #045f8a;border-color: #25718a;color: #FFFFFF;">Ingresar</button>

                           
                    </div>
                </form>
                

                
 

            <br>
        </div>

        </div>

   
 

</div>
   </div>

 

<!-- Mainly scripts -->
    {!! Html::script('admin/plantilla/js/jquery-2.1.1.js') !!}
    {!! Html::script('admin/plantilla/js/bootstrap.min.js') !!}
  
    <script type="text/javascript">
        function val(e) {
            tecla = (document.all) ? e.keyCode : e.which;
            if (tecla==8) return true;
            var patron =/[0-9]/;
            var te = String.fromCharCode(tecla);
            return patron.test(te);
        }
    </script>

</body>
</html>




