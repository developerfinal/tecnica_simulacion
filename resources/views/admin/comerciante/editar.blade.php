@extends('layouts.backend')
@section('titulo')
Actualizar Datos de comerciantes
@stop
@section('contenido')
@include('alerts.errors')
@include('alerts.request')
{!! Form::model($comerciante, array('method' => 'PATCH', 'route' => array('comerciantes.update', $comerciante->id))) !!}    
   
    <div class="form-group">
            {!! Form::label('cedula', 'Cédula:') !!}
            {!! Form::text('cedula',null,['class'=>'form-control','placeholder'=>'Ingrese la cédula']) !!}
    </div>
     <div class="form-group">
            {!! Form::label('nombre', 'Apellidos y nombres:') !!}
            {!! Form::text('nombre',null,['class'=>'form-control','placeholder'=>'Ingrese apellidos y nombres:']) !!}
    </div>
    <div class="form-group">
            {!! Form::label('telefono', 'Teléfono:') !!}
            {!! Form::text('telefono',null,['class'=>'form-control','placeholder'=>'Ingrese el telefono']) !!}
    </div>
    <div class="form-group">
            {!! Form::label('direccion', 'Dirección:') !!}
            {!! Form::text('direccion',null,['class'=>'form-control','placeholder'=>'Ingrese la dirección:']) !!}
    </div>
    <div class="form-group">
            {!! Form::label('numero_local', 'Numero de Local:') !!}
            {!! Form::text('numero_local',null,['class'=>'form-control','placeholder'=>'Ingrese el número de Local:']) !!}
    </div>
    <div class="form-group">
            {!! Form::label('valor_adeudado', 'Valor de deuda:') !!}
            {!! Form::text('valor_adeudado',0,['class'=>'form-control','placeholder'=>'Ingrese el valor adeudado:']) !!}
    </div> 
     <div class="form-group">
        {!! Form::label('mercado_id', 'Mercado    :') !!}
        {!!Form::select('mercado_id', $mercados,$mercado_seccion->mercado_id, ['class' => 'form-control'])!!}
    </div>  
    <div class="form-group">
        {!! Form::label('seccion_id', 'Sección    :') !!}
        {!!Form::select('seccion_id', $secciones,$mercado_seccion->seccion_id, ['class' => 'form-control'])!!}
    </div> 

            {!!Form::submit('Actualizar',['class'=>'btn btn-primary'])!!}


            
        
 
{!! Form::close() !!}



@stop


@section('script')
<script type="text/javascript">
    
$(document).ready(function()  {

$('#mercado_id').selectize();
$('#seccion_id').selectize();
   }); 
</script>
@stop


