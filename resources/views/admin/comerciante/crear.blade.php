@extends('layouts.backend')
@section('titulo')
Crear comerciante
@stop
@section('contenido')
@include('alerts.errors')
@include('alerts.request')
 
 {!! Form::open(['route'=>'comerciantes.store', 'method' => 'POST']) !!}
   
   
    <div class="form-group">
            {!! Form::label('cedula', 'Cédula:') !!}
            {!! Form::text('cedula',null,['class'=>'form-control','placeholder'=>'Ingrese la cédula','onkeypress'=>'return solonum(event)','maxlength'=>'10']) !!}
    </div>
     <div class="form-group">
            {!! Form::label('nombres', 'Apellidos y nombres:') !!}
            {!! Form::text('nombres',null,['class'=>'form-control','placeholder'=>'Ingrese apellidos y nombres:','onkeypress'=>'return sololet(event)','maxlength'=>'100']) !!}
    </div>
    <div class="form-group">
            {!! Form::label('telefono', 'Teléfono:') !!}
            {!! Form::text('telefono',null,['class'=>'form-control','placeholder'=>'Ingrese el telefono','onkeypress'=>'return solonum(event)','maxlength'=>'10']) !!}
    </div>
    <div class="form-group">
            {!! Form::label('direccion', 'Dirección:') !!}
            {!! Form::text('direccion',null,['class'=>'form-control','placeholder'=>'Ingrese la dirección:']) !!}
    </div>
    <div class="form-group">
            {!! Form::label('numero_local', 'Numero de Local:') !!}
            {!! Form::text('numero_local',null,['class'=>'form-control','placeholder'=>'Ingrese el número de Local:','onkeypress'=>'return solonum(event)','maxlength'=>'10']) !!}
    </div>
    <div class="form-group">
            {!! Form::label('valor_adeudado', 'Valor de deuda:') !!}
            {!! Form::text('valor_adeudado',0,['class'=>'form-control','placeholder'=>'Ingrese el valor adeudado:','onkeypress'=>'return valor(event)','maxlength'=>'10']) !!}
    </div> 
     <div class="form-group">
        {!! Form::label('mercado_id', 'Mercado    :') !!}
        {!!Form::select('mercado_id', $mercados, ['class' => 'form-control'])!!}
    </div>  
    <div class="form-group">
        {!! Form::label('seccion_id', 'Sección    :') !!}
        {!!Form::select('seccion_id', $secciones, ['class' => 'form-control'])!!}
    </div> 

            {!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!}


            
        
 
{!! Form::close() !!}



@stop


@section('script')
<script type="text/javascript">
    
$(document).ready(function()  {

$('#mercado_id').selectize();
$('#seccion_id').selectize();
   }); 
</script>
@stop


