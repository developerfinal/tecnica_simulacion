@extends('layouts.backend')

@section('ruta')
Listado de Doctores
@stop

@section('titulo')
Comerciantes exonerados
@stop
@section('contenido')
@include('alerts.errors')
@include('alerts.success')
@include('alerts.request') 
        <div class="input-group">
            <input type="text"  id="txtclave" placeholder="Ingrese Cedula (Sin guión)"  class="form-control input-lg">
            <div class="input-group-btn">
                <button class="btn btn-lg btn-primary"  id="btnbuscar" data-toggle="tooltip"  title="Buscar paciente">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </button>
             </div>
        </div>    
   <div id='detalles'  class="table-responsive"></div>
<br>  
@if (count($exonerados))
<center><h1>Comerciantes Exonerados</h1></center>
<table  id="tbbuzon" class="table table-striped table-bordered table-hover display" >
        <thead>
            <tr>       
                <th>Cedula</th>                       
                <th>Apellidos y nombres</th>      
                <th>NUmero de Local</th>   
                <th width="10%">Opciones</th>
            </tr>
        </thead>
        <tbody>
            <?php $s = 0 ; 
            ?>
            @foreach ($exonerados as $exonerado) 
            <tr>      
              <td>{{ $exonerado->cedula }}</td>                       
              <td>{{ $exonerado->nombre }}</td>      
              <td>{{ $exonerado->numero_local }}</td>                   
              <td style="display: inline-flex; float: right;" colspan="2">         
                 {!! Form::open(array('method' => 'DELETE', 'route' => array('delete_exonerado', $exonerado->id))) !!}   
                 <button class="btn btn-danger inline"   data-toggle="tooltip"  title="Eliminar de Asociación"><i class="fa fa-trash-o"></i></button>
                  {!! Form::close() !!}
              </td>
            </tr>
            @endforeach              
        </tbody>      
    </table>
@else
<center><h1>Aun no cuenta con exonerados </h1></center>
@endif
@stop
@Section('script')
<script>
$(document).ready(function(){
  //alert('cargado');
});
    $("#btnbuscar").click(function (){  
  var cedula_ruc=$('#txtclave').val();
  
  if (cedula_ruc==null||cedula_ruc=="") 
  {
       $('#pop_up_error_ingreso').modal('show');
  }else
  {
    $.ajax({
         type:"GET",
          url:"{{url('/')}}/consultar_exon/"+cedula_ruc,
          dataType:"json",
         contentType:"text/plain",
      }).done(function(msg){
        //console.log(msg);
         arreglo2=msg;
         if (arreglo2.length==0) {
            var gene1="<div class='container table-responsive'>";
               gene1+="<h1 class='text-center'>No hay comerciantes ingresados</h1>"
            gene1+="</div>";
            $('#detalles').html(gene1);
         }else{
              var elemen=arreglo2;
              gene="<table  class='table table-striped table-bordered table-hover display'>";
              gene+='<thead>';
              gene+="<tr>";    
              gene+="<th width='10%'><center>Cédula</center></th>";
              gene+="<th width='50%' ><center> Nombres y Apellidos </center></th>";
              gene+="<th width='50%' ><center> Número de Local </center></th>";
              gene+="<th width='5%'><center>Acciones</center></th>";
              gene+="</tr>";
              gene+='</thead>';
              gene+='<tbody>';
              msg.forEach(function(elemen){
              gene+='<tr>';
              gene+='<td>'+elemen.cedula+'</td>';
              gene+='<td>'+elemen.nombres+'</td>';
              gene+='<td>'+elemen.numero_local+'</td>';
              var add_direccion="{{url('add_miembro_area')}}/"+elemen.id ;    
              gene+='<td><a  class="btn btn-success" data-toggle="tooltip" onclick="agregar_exonerado('+elemen.id+')"  title="Añadir a area Médica"><i class="fa fa-plus" aria-hidden="true"></i></a>';
              gene+='</tr>'; 
             })
              gene+='</tbody>';
        
         gene+='</table>';
         gene+='</div></div>';
         $('#detalles').html(gene);        
         }
//hacer nuevamente la peticion 
      }).error(function(error){
        console.log(error);
      });
  }
});
    function agregar_exonerado(id)
        {
           $.ajax({
          url : '{{ route('add_exonerado') }}',
          type: "POST",
          data : {id: id,"_token": "{{ csrf_token() }}"},
        })
        .done(function(datax) {
          if(datax==1)
          {
            location.reload();
          }
        })
          .fail(function(datax) {
            console.log(datax);
          })
          .always(function(datax) {
            console.log('Terminada la peticion');
          });
        }

        
</script>
@stop
