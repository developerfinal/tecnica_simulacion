@extends('layouts.backend')

@section('ruta')
Listado de comerciantes
@stop

@section('titulo')
Comerciantes
@stop
@section('contenido')
@include('alerts.errors')
@include('alerts.success')
@include('alerts.request')
<a class="btn btn-primary" href="{{ URL::route('comerciantes.create') }}">Nuevo</a>
<style>
  #gif_cargando
{
  display: none;
}
#txt_seguro
{
  padding: 8px; font-size: 15px; width: 100%; font-weight: bold; border: none; 
}
</style>
       <div class="table-responsive">
      <table  id="tb_comerciantes" class="table table-striped table-bordered table-hover display" >
       
        <thead>
            <tr>
                <th>id</th>                             
                <th>Apellidos y nombres</th>               
                <th>Cédula</th>
                <th>Telefono</th> 
                <th>Dirección</th>       
		            <th>Número del local</th>
                <th>Valor adeudado</th>
                <th>Estado</th>
                <th>Mercado</th>
                <th>Sección</th>      
                <th width="10%">Opciones</th> 
            </tr>
        </thead>
    </table>
  </div>
@stop
@section('script')
<script>
  $(document).ready(function(){
    $(".inline").colorbox();
    ListarTabla();
    });
  function ListarTabla()
  {
      var table= $('#tb_comerciantes').dataTable( {
         "language": {
          "emptyTable": "No hay comerciantes generados",
          "search":"Buscar",
             "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
        },
      "ajax": {
        "url": "{{ url('comerciantes_ajax') }}"
      },
      "columns":[
        {"data":"id"},
        {"data":"nombre"},
        {"data":"cedula"},
        {"data":"telefono"},
        {"data":"direccion"},        
        {"data":"numero_local"},
        {"data":"valor_adeudado"},        
        {"data":"estado"},
        {"data":"seccion"},
        {"data":"mercado"},
        {"defaultContent":'<button type="submit"  class="btn btn-success bnt-lg editar"   data-toggle="tooltip"  title="Editar"><i class="fa fa-pencil-square-o"></i></button><button type="submit" class="btn btn-danger bnt-lg eliminar" onclick="SeguroEliminar()"  data-toggle="tooltip"  title="Eliminar"><i class="fa fa-trash"></i></button>'}
        ]
    });
    editar_comerciante("#tb_comerciantes",table);
  }
  var editar_comerciante=function(tbody,table)
  {
    $(tbody).on("click","button.editar",function(){
      var data=table.api().row($(this).parents("tr")).data();
      console.log(data);
      location.href="{{ url('comerciantes') }}/"+data.id+"/edit"
    });
    $(tbody).on("click","button.eliminar",function(){
      var data=table.api().row($(this).parents("tr")).data();
      console.log(data);
      $.colorbox({width:"auto",height:"auto", open:true, href:"seguro_eliminar/comerciantes/"+data.id});
    });
  }


    $(function () {
      $('[data-toggle="tooltip"]').tooltip('show')
    })
   

</script>
@stop