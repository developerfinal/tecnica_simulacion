@extends('layouts.backend')

@section('titulo')
Editar Paciente
@stop


@section('contenido')

@include('alerts.request')
 


{!! Form::model($mercados, array('method' => 'PATCH', 'route' => array('mercados.update', $mercados->id) )) !!}
    
     
<div class="form-group">
           {!! Form::label('descripcion', 'descripcion:') !!}
            {!! Form::text('descripcion',null,['class'=>'form-control','placeholder'=>'Ingrese una descripcion']) !!}
    </div>
    

            {!!Form::submit('Actualizar',['class'=>'btn btn-primary'])!!}


            
        
 
{!! Form::close() !!}



@stop