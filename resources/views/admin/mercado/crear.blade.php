@extends('layouts.backend')

@section('titulo')
Nuevo Mercado
@stop

@section('contenido')

@include('alerts.request')


{!! Form::open(['route' => 'mercados.store', 'method' => 'POST']) !!}   
   
<div class="form-group">
            {!! Form::label('descripcion', 'Mercado:') !!}
            {!! Form::text('descripcion',null,['class'=>'form-control','placeholder'=>'Ingrese una descripción']) !!}
    </div>
   

            {!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!}


            
        
 
{!! Form::close() !!}



@stop