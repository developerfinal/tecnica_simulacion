@extends('layouts.backend')

@section('ruta')
Listado de Mercados
@stop

@section('titulo')
Mercados
@stop

@section('contenido')
@include('alerts.errors')
@include('alerts.success')
@include('alerts.request')

 

<a class="btn btn-primary" href="{{ URL::route('mercados.create') }}">Nuevo</a>
@if (count($mercados))
<div class="table-responsive">
     <table  id="tbbuzon1" class="table table-striped table-bordered table-hover display" >
        <thead>
            <tr>
              <th>Id</th>
                <th>mercado</th>
                
                <th width="10%">Opciones</th>
            </tr>
        </thead>

        <tbody>
            <?php $s = 0 ; ?>
            @foreach ($mercados as $mercado)

                <tr>
                  <td>{{ $mercado->id }}</td>
                	<td>{{ $mercado->descripcion }}</td>
                             
                    <td style="display: inline-flex; float: right;">
                        {!! Form::open(array('method' => 'GET', 'route' => array('mercados.edit', $mercado->id))) !!}   
                      <button type="submit" class="btn btn-success bnt-lg"   data-toggle="tooltip"  title="Editar">
                      <i class="fa fa-pencil-square-o"></i></button>
                      {!! Form::close() !!}
                      <button type="submit" class="btn btn-danger bnt-lg" onclick="SeguroEliminar({{$mercado->id}})"  data-toggle="tooltip"  title="Eliminar"><i class="fa fa-trash"></i></button>
                     </td>
                </tr>
            @endforeach
              
        </tbody>
      
    </table>
  </div>
    
    
    
@else
    No hay mercado
@endif

@stop
@section('script')
<script>
  $(document).ready(function(){
    $(".inline").colorbox();
  //  $(".add_usser").colorbox();
    });
    $(function () {
      $('[data-toggle="tooltip"]').tooltip('show')
    })
    function SeguroEliminar(id)
   {
    $.colorbox({width:"auto",height:"auto", open:true, href:"seguro_eliminar/mercados/"+id});
   }

</script>
@stop