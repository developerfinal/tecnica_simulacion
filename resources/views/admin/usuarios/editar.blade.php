@extends('layouts.backend')
@section('titulo')
Actualizar Datos
@stop
@section('contenido')
@include('alerts.errors')
@include('alerts.request')
{!! Form::model($usuario, array('method' => 'PATCH', 'route' => array('usuarios.update', $usuario->id))) !!}    
   
        
       
<div class="form-group">
            {!! Form::label('cedula', 'Cedula:') !!}
            {!! Form::text('cedula',null,['class'=>'form-control','placeholder'=>'Ingrese su Cedula']) !!}
    </div>
    <div class="form-group">
            {!! Form::label('name', 'Nombre de usuario:') !!}
            {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Ingrese un nombre de usuario']) !!}
    </div>

    <div class="form-group">
            {!! Form::label('email', 'Correo electronico:') !!}
            {!! Form::text('email',null,['class'=>'form-control','placeholder'=>'Ingrese el email:']) !!}
    </div>
    <div class="form-group">
            {!! Form::label('telefono', 'telefono:') !!}
            {!! Form::text('telefono',null,['class'=>'form-control','placeholder'=>'Ingrese telefono']) !!}
    </div>
    <div class="form-group">
            {!! Form::label('fecha_nacimiento', 'Fecha de nacimiento:') !!}
            {!! Form::text('fecha_nacimiento',date('d-m-Y', strtotime($usuario->fecha_nacimiento)),['class'=>'form-control','readonly']) !!}
    </div>   
    
     <div class="form-group">
        {!! Form::label('id_tipo_usuario', 'tipo usuario    :') !!}
        {!!Form::select('id_tipo_usuario', $tipos,$usuario->id_tipo_usuario, ['class' => 'form-control',])!!}
    </div>

 <div class="form-group">
        {!! Form::label('mercado_id', 'tipo mercado    :') !!}
        {!!Form::select('mercado_id', $mercado,$usuario->mercado_id, ['class' => 'form-control'])!!}
    </div>
    <div class="form-group">
        {!! Form::label('password', 'Contraseña    :') !!}
        {!!Form::password('password', null, ['class' => 'form-control'])!!}
    </div>
     {!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!}


            
        
 
{!! Form::close() !!}



@stop

@section('script')
<script type="text/javascript">
    
$(document).ready(function()  {

$('#id_tipo_usuario').selectize();

$('#mercado_id').selectize();
         $('#fecha_nacimiento').datepicker({
            format: "dd-mm-yyyy",
            startDate: "01/01/1970",
            endDate: "{{date('d/m/Y')}}",
            clearBtn: true,
            language: "es",
            autoclose: true,
            todayHighlight: true,
            todayBtn: "linked"
          }); 
   }); 
</script>
@stop