@extends('layouts.backend')
@section('titulo')
Crear usuario
@stop
@section('contenido')
@include('alerts.errors')
@include('alerts.request')
 
 {!! Form::open(['route'=>'usuarios.store', 'method' => 'POST']) !!}

   
        
       
<div class="form-group">
            {!! Form::label('cedula', 'Cedula:') !!}
            {!! Form::text('cedula',null,['class'=>'form-control','placeholder'=>'Ingrese su Cedula','onkeypress'=>'return solonum(event)','maxlength'=>'10']) !!}


    </div>
    <div class="form-group">
            {!! Form::label('name', 'Nombre de usuario:') !!}
            {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Ingrese un nombre de usuario','onkeypress'=>'return sololet(event)','maxlength'=>'100']) !!}
    </div>

    <div class="form-group">
            {!! Form::label('email', 'Correo electronico:') !!}
            {!! Form::text('email',null,['class'=>'form-control','placeholder'=>'Ingrese el email:','onkeypress'=>'return soloemail(event)','maxlength'=>'50']) !!}
    </div>
    <div class="form-group">
            {!! Form::label('telefono', 'telefono:') !!}
            {!! Form::text('telefono',null,['class'=>'form-control','placeholder'=>'Ingrese telefono','onkeypress'=>'return solonum(event)','maxlength'=>'10']) !!}
    </div>
    <div class="form-group">
            {!! Form::label('fecha_nacimiento', 'Fecha de fecha de nacimiento:') !!}
            {!! Form::Date('fecha_nacimiento',null,['class'=>'form-control','placeholder'=>'Ingrese fecha de nacimiento']) !!}
    </div>  
    
     <div class="form-group">
        {!! Form::label('id_tipo_usuario', 'tipo usuario    :') !!}
        {!!Form::select('id_tipo_usuario', $tipos, ['class' => 'form-control'])!!}
    </div>

 <div class="form-group">
        {!! Form::label('mercado_id', 'tipo mercado    :') !!}
        {!!Form::select('mercado_id', $mercado, ['class' => 'form-control'])!!}
    </div>

         <div class="form-group">
        {!! Form::label('password', 'Contraseña:') !!}
        <br>
            {!! Form::password('password',['class' => 'form-control awesome','placeholder'=>'Escriba una contraseña','required']) !!}
        
    </div>



     

     {!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!}


            
        
 
{!! Form::close() !!}



@stop

@section('script')
<script type="text/javascript">
    
$(document).ready(function()  {

$('#id_tipo_usuario').selectize();
$('#mercado_id').selectize();

   }); 
</script>
@stop






