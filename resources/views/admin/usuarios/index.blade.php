@extends('layouts.backend')

@section('ruta')
Listado de Usuarios
@stop

@section('titulo')
Usuarios
@stop
@section('contenido')
@include('alerts.errors')
@include('alerts.success')
@include('alerts.request')
<a class="btn btn-primary" href="{{ URL::route('usuarios.create') }}">Nuevo</a>
@if (count($usuarios))
<style>
  #gif_cargando
{
  display: none;
}
#txt_seguro
{
  padding: 10px; font-size: 15px; width: 100%; font-weight: bold; border: none; 
}
</style>
    <div class="table-responsive">
    <table  id="tbbuzon1" class="table table-striped table-bordered table-hover display" >
        <thead>
            <tr>
               <th>IId</th>
                <th>cedula</th>
                <th>Nombre de usuario</th>  
                <th>email</th> 
                <th>telefono</th>   
                <th>fecha de nacimiento </th>     
                <th>Tipo_Usuario</th>       
		            <th>Mercado</th>                                    
                <th width="10%">Opciones</th>
            </tr>
        </thead>

        <tbody>
            <?php $s = 0 ; ?>
            @foreach ($usuarios as $cliente)   
                  <td>{{ $cliente->id }}</td>  
                  <td>{{ $cliente->cedula }}</td>   
                  <td>{{ $cliente->name }}</td>  
                  <td>{{ $cliente->email }}</td> 
                	<td>{{ $cliente->telefono }}</td>
                  <td>{{ $cliente->fecha_nacimiento }}</td> 
                  <td>{{ $cliente->tipo_usuario }}</td>
                  <td>{{ $cliente->mercado_id }}</td>     
			            <td style="display: inline-flex; float: right;">
                      {!! Form::open(array('method' => 'GET', 'route' => array('usuarios.edit', $cliente->id))) !!}   
                      <button type="submit" class="btn btn-success bnt-lg"   data-toggle="tooltip"  title="Editar">
                      <i class="fa fa-pencil-square-o"></i></button>
                      {!! Form::close() !!}
                      <button type="submit" class="btn btn-danger bnt-lg" onclick="SeguroEliminar({{$cliente->id}})"  data-toggle="tooltip"  title="Eliminar"><i class="fa fa-trash"></i></button>
               </td>
                </tr>
            @endforeach
        </tbody>
    </table>
  </div>
@else
    No hay Mercados
@endif
@stop
@section('script')
<script>
  $(document).ready(function(){
    $(".inline").colorbox();
  //  $(".add_usser").colorbox();
    });
    $(function () {
      $('[data-toggle="tooltip"]').tooltip('show')
    })
    function SeguroEliminar(id)
   {
    $.colorbox({width:"auto",height:"auto", open:true, href:"seguro_eliminar/usuarios/"+id});
   }

</script>
@stop