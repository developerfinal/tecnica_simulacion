

  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
      <h2 class="modal-title"><center>¿Esta seguro de Eliminar al Usuario?</center></h2>
      </div>
      <div class="modal-body" data-spy="scroll" data-target=".bs-docs-sidebar" style="overflow-y:auto; width: default; ">
<div class="form-group" align="center">
        {!! Form::open(array('method' => 'DELETE', 'route' => array('usuarios.destroy', $id))) !!}  
        <button type="submit" class="btn btn-danger">Eliminar</button>
        <a href="usuarios" class="btn btn-primary" data-dismiss="modal" id="btnSig2">Cancelar</a>     
</div>        
      </div>
      <div class="modal-footer">
      <enter> 
        
        {!! Form::close() !!}
      </enter>       
      </div>
    </div>
  </div> 

