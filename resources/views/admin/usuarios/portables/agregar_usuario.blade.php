}<div  id="pop_up_solicitar" data-backdrop="static" data-keyboard="false" >
  <div class="modal-dialog modal-md">
    <div class="modal-content">    
            {!! Form::open(['route'=>'usuarios.store', 'method' => 'POST']) !!} 
      <div class="modal-header">
 
        <h2 class="modal-title"><center>Añadir nuevo usuario al sistema</center></h2>
      </div>
      <div class="modal-body" data-spy="scroll" data-target=".bs-docs-sidebar" style="overflow-y:auto; width: default;">

    <div class="row">  
      <div class="col-md-6"> 
            <label for="cedula_ruc:name"><span style="color: red;margin-bottom: 0px;font-size: 11px;font-style: italic;text-align: right;margin-top: 0px;" id="mensaje"> </span> </label>
            {!! Form::text('username',null,['title'=>'Sin guión, Ejemplo: 1303497801','data-placement'=>'top','data-toggle'=>'tooltip','class'=>'form-control','placeholder'=>'Username', 'onkeypress'=>'return val(event)', 'maxlength'=>'20','required']) !!}
        </div>

        <div class="col-md-6"> 
            <label for="cedula_ruc:name"><span style="color: red;margin-bottom: 0px;font-size: 11px;font-style: italic;text-align: right;margin-top: 0px;" id="mensaje"> </span> </label>
            {!! Form::text('cedula_ruc',null,['title'=>'Sin guión, Ejemplo: 1303497801','data-placement'=>'top','data-toggle'=>'tooltip','class'=>'form-control','placeholder'=>'Cédula de identidad', 'id'=>'txtcedula', 'onkeypress'=>'return val(event)', 'maxlength'=>'10','required']) !!}
        </div> 

        <div class="col-md-6">
        <br>
           {!! Form::text('apellidos',null,['title'=>'Escriba sus dos apellidos','data-placement'=>'top','data-toggle'=>'tooltip','class'=>'form-control','placeholder'=>'Apellidos', 'onkeypress'=>'return val2(event)', 'maxlength'=>'100', 'id'=>'txtapellido','required']) !!}
        </div>

        <div class="col-md-6">
         <br>
            {!! Form::text('nombres',null,['title'=>'Escriba sus dos nombres','data-placement'=>'top','data-toggle'=>'tooltip','class'=>'form-control','placeholder'=>'Nombres', 'onkeypress'=>'return val2(event)', 'maxlength'=>'100', 'id'=>'txtnombre','required']) !!}
        </div>

        <div class="col-md-6">
        <br>
           {!! Form::text('email',null,['class'=>'form-control','placeholder'=>'Correo electrónico', 'maxlength'=>'50','required']) !!}
        </div>   
   
        <div class="col-md-6">
        <br>
        {!! Form::password('password',['class' => 'form-control awesome','placeholder'=>'Escriba una contraseña','required']) !!}
        </div>    

    </div>
        <div class="row">
        <div class="col-md-6">
        {!! Form::label('tipo_usuario', 'Tipo de Usuario del sistema     :') !!}
        {!!Form::select('tipo_usuario', $tipos, ['class' => 'form-control'])!!}
        </div>
        <div class="col-md-6">
       {!! Form::label('subcentro', 'Centro de Atención:') !!}
        {!!Form::select('subcentro',$subcentros, ['class' => 'form-control'])!!}
        </div> 
          <div class="col-md-12">
       {!! Form::label('area_medica_id', 'Áreas medicas:') !!}
       <br>
        {!!Form::select('area_medica_id',$areas_medicas, ['class' => 'form-control'])!!}
        </div>
        </div>
    <div class="modal-footer">
         <button type="submit" class="btn btn-primary">Aceptar</button>
    </div>      
         {!! Form::close() !!} 
  </div> 
</div>