@extends('layouts.backend')
@section('titulo')
Tipos de Usuarios
@stop
@section('contenido')
@include('alerts.success')
@include('alerts.errors')
@if (count($tipos))
<a class="btn btn-primary" href="{{ URL::route('tipousuarios.create') }}">Nuevo</a>
<div class="table-responsive">
    <table  id="tbbuzon1" class="table table-striped table-bordered table-hover display" >
           <thead>
            <tr>
                <th>ID:</th>
                <th>TIPO:</th>
                <th>estado:</th>
                <th width="10%">Opciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($tipos as $tipo)
                <tr>                 
                	<td>{{$tipo->id}}</td>
                  <td>{{$tipo->tipo_usuario}}</td>
                  <td>{{$tipo->estado}}</td>
                   <td style="display: inline-flex; float: right;">
                    {!! Form::open(array('method' => 'GET', 'route' => array('tipousuarios.edit', $tipo->id))) !!}   
                      <button type="submit" class="btn btn-success bnt-lg"   data-toggle="tooltip"  title="Editar">
                      <i class="fa fa-pencil-square-o"></i></button>
                      {!! Form::close() !!}                      
                      <button type="submit" class="btn btn-danger bnt-lg" onclick="SeguroEliminar({{$tipo->id}})"  data-toggle="tooltip"  title="Eliminar"><i class="fa fa-trash"></i></button>
                  </td>     
                </tr>
            @endforeach              
        </tbody>    
        </table>   
        </div>   
@else

Aún no hay tipos de usuarios en el sistema


@endif
@stop
@section('script')
<script>
  $(document).ready(function(){
    $(".inline").colorbox();
  //  $(".add_usser").colorbox();
    });
    $(function () {
      $('[data-toggle="tooltip"]').tooltip('show')
    })
    function SeguroEliminar(id)
   {
    $.colorbox({width:"auto",height:"auto", open:true, href:"seguro_eliminar/tipousuarios/"+id});
   }

</script>
@stop