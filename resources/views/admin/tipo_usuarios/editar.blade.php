@extends('layouts.backend')
@section('titulo')
Actualizar tipo de usuario
@stop
@section('contenido')
@include('alerts.errors')
@include('alerts.request')
 
{!! Form::model($tipo_usuario, array('method' => 'PATCH', 'route' => array('tipousuarios.update', $tipo_usuario->id))) !!} 
    <div class="form-group">
            {!! Form::label('Tipo de Usuario', 'Nombre del Nuevo tipo de Usuario') !!}
            {!! Form::Text('tipo',null,['class'=>'form-control', 'placeholder'=>'Ingrese el Nombre del Nuevo tipo de Usuario']) !!}
    </div>
    <div class="form-group">
            {!! Form::label('descripcion', 'Descripcion') !!}
            {!! Form::Text('descripcion',null,['class'=>'form-control','placeholder'=>'Ingrese una pequeña Descripcion de los permiso y lo que puede hacer este Usuario.']) !!}
    </div>      
            {!!Form::submit('Actualizar Tipo de Usuario',['class'=>'btn btn-primary'])!!}
{!! Form::close() !!}
@stop