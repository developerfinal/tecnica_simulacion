@extends('layouts.backend')
@section('titulo')
Crear tipo de usuario
@stop
@section('contenido')
@include('alerts.errors')
@include('alerts.request')
 
 {!! Form::open(['route'=>'tipousuarios.store', 'method' => 'POST']) !!}    
    <div class="form-group">
            {!! Form::label('Tipo de Usuario', 'Nombre del Nuevo tipo de Usuario') !!}
            {!! Form::Text('tipo',null,['class'=>'form-control', 'placeholder'=>'Ingrese el Nombre del Nuevo tipo de Usuario','onkeypress'=>'return sololet(event)','maxlength'=>'50']) !!}
    </div>
    <div class="form-group">
            {!! Form::label('descripcion', 'Descripcion') !!}
            {!! Form::Text('descripcion',null,['class'=>'form-control','placeholder'=>'Ingrese una pequeña Descripcion de los permiso y lo que puede hacer este Usuario.']) !!}
    </div>      
            {!!Form::submit('Registrar Tipo de Usuario',['class'=>'btn btn-primary'])!!}
{!! Form::close() !!}
@stop