@extends('layouts.backend')
@section('titulo')
Actualizar Sección
@stop
@section('contenido')
@include('alerts.errors')
@include('alerts.request')


{!! Form::model($seccion, array('method' => 'PATCH', 'route' => array('seccion.update', $seccion->id))) !!}
                <div class="form-group">
                    {!! Form::label('nombre','Sección:',array("class"=>"col-lg-3 control-label")) !!}
                  <div class="col-lg-8"> 
                    {!! Form::text('nombre', Input::old('nombre',$seccion->nombre) , array('class' => 'form-control','placeholder'=>'Ingrese Sección')) !!}
                  </div>
                </div>

                <div class="row">
                    <div class="col-lg-11">
                        <table class="table table-bordered table-striped table-hover" class="centro">
                            <thead>
                                <tr>
                                    <th colspan="3" class="centro">Seleccione los Mercados en donde se encuentra la sección</th>
                                </tr>
                                <tr>
                                    <th class="centro">Marque la casilla si la seccion pertenece al Mercado</th>
                                    <th class="centro">Mercado</th>
                                    <th class="centro">Valor</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($mercados as $mercado)
                                    <?php
                                        $bandera = 0;
                                        $valor_tar = '';
                                        foreach ($seccion->mercados as $merc) {
                                            if ($merc->id == $mercado->id) {
                                                $bandera = 1;
                                                $valor_tar = $merc->pivot->valor;
                                            }
                                        }
                                    ?>
                                    <tr>
                                        <td>
                                            <input type="checkbox" name="mercados[]" value="{{ $mercado->id }}" 
                                                @if ($bandera == 1)
                                                    checked 
                                                @endif
                                            />

                                        </td>
                                        <td>
                                            {{ $mercado->descripcion }}
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="valor_mercado{{ $mercado->id }}" id="valor_mercado{{ $mercado->id }}" value="{{ $valor_tar }}" />
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                {!!  Form::submit("Guardar", array('class' => 'btn btn-primary', 'style' => 'float:right' ))  !!}
            {!!  Form::close()  !!}



@stop

@section('script')
<script type="text/javascript">
    
$(document).ready(function()  {

$('#id_tipo_usuario').selectize();

$('#mercado_id').selectize();
         $('#fecha_nacimiento').datepicker({
            format: "dd-mm-yyyy",
            startDate: "01/01/1970",
            endDate: "{{date('d/m/Y')}}",
            clearBtn: true,
            language: "es",
            autoclose: true,
            todayHighlight: true,
            todayBtn: "linked"
          }); 
   }); 
</script>
@stop
