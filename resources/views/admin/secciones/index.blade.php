@extends('layouts.backend')

@section('ruta')
Listado de Usuarios
@stop

@section('titulo')
Secciones
@stop
@section('contenido')
@include('alerts.errors')
@include('alerts.success')
@include('alerts.request')
<a class="btn btn-primary" href="{{ URL::route('seccion.create') }}">Nuevo</a>
@if (count($secciones))
<div class="table-responsive">
    <table id="tbbuzon1" class="table table-striped table-bordered table-hover display" >
                <thead>
                    <tr>
                       <th>ID</th>
                       <th>Sección</th>
                       <th width="10%">Acción</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($secciones as $seccion)
                        <tr>
                            <td>{{ $seccion->id }}</td>
                            <td>{{ $seccion->nombre }}</td>
                             <td style="display: inline-flex; float: right;">
                                  {!! Form::open(array('method' => 'GET', 'route' => array('seccion.edit', $seccion->id))) !!}   
                                  <button type="submit" class="btn btn-success bnt-lg"   data-toggle="tooltip"  title="Editar">
                                  <i class="fa fa-pencil-square-o"></i></button>
                                  {!! Form::close() !!}
                                  <button type="submit" class="btn btn-danger bnt-lg" onclick="SeguroEliminar({{$seccion->id}})"  data-toggle="tooltip"  title="Eliminar"><i class="fa fa-trash"></i></button>
                           </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
          </div>
@else
    No hay Secciones
@endif
@stop
@section('script')
<script>
  $(document).ready(function(){
    $(".inline").colorbox();
  //  $(".add_usser").colorbox();
    });
    $(function () {
      $('[data-toggle="tooltip"]').tooltip('show')
    })
    function SeguroEliminar(id)
   {
    $.colorbox({width:"auto",height:"auto", open:true, href:"seguro_eliminar/seccion/"+id});
   }

</script>
@stop