@extends('layouts.backend')
@section ('titulo')
	Sección
@endsection
@section('estilos')
	<style type="text/css">
		.centro {
			text-align: center;
		}
	</style>
@endsection
@section('contenido')

@include('alerts.errors')
@include('alerts.request')

		 {!! Form::open(['route'=>'seccion.store', 'method' => 'POST']) !!}
				<div class="form-group">
                    {!! Form::label('nombre','Sección:',array("class"=>"col-lg-3 control-label")) !!}
                  <div class="col-lg-8"> 
                    {!! Form::text('nombre', Input::old('nombre') , array('class' => 'form-control','placeholder'=>'Ingrese Sección')) !!}
                  </div>
                </div>

                <div class="row">
                	<div class="col-lg-11">
                		<table class="table table-bordered table-striped table-hover" class="centro">
                			<thead>
                				<tr>
                					<th colspan="3" class="centro">Seleccione los Mercados en donde se encuentra la sección</th>
                				</tr>
                				<tr>
                					<th class="centro">Marque la casilla si la seccion pertenece al Mercado</th>
                					<th class="centro">Mercado</th>
                					<th class="centro">Valor</th>
                				</tr>
                			</thead>
                			<tbody>
                				@foreach ($mercados as $mercado)
                					<tr>
                						<td>
                							<input type="checkbox" name="mercados[]" value="{{ $mercado->id }}"	/>

                						</td>
                						<td>
                							{{ $mercado->descripcion }}
                						</td>
                						<td>
                							<input type="text" class="form-control" name="valor_mercado{{ $mercado->id }}" id="valor_mercado{{ $mercado->id }}" />
                						</td>
                					</tr>
                				@endforeach
                			</tbody>
                		</table>
                	</div>
                </div>
                {!!  Form::submit("Guardar", array('class' => 'btn btn-primary', 'style' => 'float:right' ))  !!}
            {!!  Form::close()  !!}
@endsection
@section ('scripts')
	<script type="text/javascript">
		$(document).ready(function() {});
	</script>
@endsection