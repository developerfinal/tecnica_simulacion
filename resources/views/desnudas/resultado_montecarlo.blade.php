<div class="col-md-6">
	<table class="table table-bordered table-hover table-striped">
		<tr>
			<th colspan="5" align="center">Tabla de probabilidades de demandas</th>
		</tr>
		<tr>
			<th style="text-align: center;">#</th>
			<th style="text-align: center;">Demanda</th>
			<th style="text-align: center;">Probabilidad</th>
			<th style="text-align: center;">Acumulada</th>
			<th style="text-align: center;">Menor</th>
			<th style="text-align: center;">Mayor</th>
		</tr>
		<?php 
		$total_n=0;
		$total_p=0;
		$total_po=0;
		?>
		@for ($i = 0; $i < count($tabla_pro_demanda); $i++)
		<?php
		$total_n=$total_n+ $tabla_pro_demanda[$i][0];
		$total_p=$total_p+$tabla_pro_demanda[$i][1];
		$prop=round($tabla_pro_demanda[$i][0]/$total,3);
		$total_po=$total_po+$prop;
		?>
			<tr>
				<td style="text-align: center;">{{ $i+1 }}</td>
				<td style="text-align: center;">{{ $tabla_pro_demanda[$i][0] }}</td>
				<td style="text-align: center;">{{ $tabla_pro_demanda[$i][1] }} | {{ $prop }}</td>
				<td style="text-align: center;">{{ $tabla_pro_demanda[$i][2] }}</td>
				<td style="text-align: center;">{{ $tabla_pro_demanda[$i][3] }}</td>
				<td style="text-align: center;">{{ $tabla_pro_demanda[$i][4] }}</td>
			</tr>
		@endfor

		<tr>
				<td></td>
				<td style="text-align: center;">{{ $total_n }}</td>
				<td style="text-align: center;">{{ $total_p }} | {{ $total_po }}</td>

				<td></td>
				<td></td>
				<td>	</td>
			</tr>
	</table>
	
</div>

<div class="col-md-6">
	<table class="table table-bordered table-hover table-striped">
		<tr>
			<th colspan="5" align="center">Simulación</th>
		</tr>
		<tr>
			<th style="text-align: center;">Evento</th>
			<th style="text-align: center;">Aleatorio</th>
			<th style="text-align: center;">Simulación</th>
		</tr>
		@for ($i = 0; $i < count($aleatorios_llegada); $i++)

			<tr>
				<td style="text-align: center;">{{ $i}}</td>
				<td style="text-align: center;">{{ $aleatorios_llegada[$i] }}</td>
				<td style="text-align: center;">
					@for ($j = 0; $j < count($tabla_pro_demanda); $j++)

					@if($aleatorios_llegada[$i]>=$tabla_pro_demanda[$j][3]&&$aleatorios_llegada[$i]<=$tabla_pro_demanda[$j][4])
					{{ $tabla_pro_demanda[$j][0] }}
					@endif
					@endfor
				</td>
				
			</tr>
		@endfor
	</table>
	
</div>
