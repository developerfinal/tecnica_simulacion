<div class="col-md-12">
	<label>
		@if ($tipo == 1)
			Probabilidades de llegada
		@else
			Probabilidades de servicio
		@endif
	</label>
	<table class="table" id="probabilidad{{ $tipo }}">
		<tr>
			<th>Valor</th>
			<th>Probabilidad</th>
		</tr>
		@for ($i = 0; $i < $valor_n; $i++)
			<tr>
				<td>
					<input type="text" class="form-control" name="valor{{ $tipo }}[]" placeholder="Valor" />
				</td>
				<td>
					<input type="text" class="form-control" name="probabilidad{{ $tipo }}[]" placeholder="Probabilidad" />
				</td>
			</tr>
		@endfor
	</table>
</div>