<div class="col-md-12">
	<label>
		@if ($tipo == 1)
			Probabilidades de llegada
		@else
			Probabilidades de Demanda
		@endif
	</label>
	<table class="table" id="probabilidad{{ $tipo }}">
		<tr>
			<th>Valor</th>
			<th>Probabilidad</th>
		</tr>
		@for ($i = 0; $i < $valor_n; $i++)
			<tr>
				<td>
					<input type="text" class="form-control" name="valor{{ $tipo }}[]" placeholder="Valor" />
				</td>
				<td>
					<input type="text" class="form-control" name="probabilidad{{ $tipo }}[]" placeholder="Probabilidad" />
				</td>
			</tr>
		@endfor
	</table>
</div>
<div class="col-md-12">
	<button class="btn btn-primary btn-md" type="button" onclick="return calcular_linea_espera();">Calcular</button>
</div>
<div id="resultado_demanda"></div>