<?php




Route::get('/', 'FrontendController@index');
// NUMERO ALEATORIO
Route::resource('numeros_aleatorios','num_aleatorioController');
Route::get('generar_tablas_aleatorios','num_aleatorioController@generar_tablas_aleatorios');
Route::get('generar_linea_tabla','lineaespera@generar_linea_tabla');
Route::resource('linea-espera','lineaespera');
Route::get('linea-espera-calculo','lineaespera@calcular_linea_espera');
Route::get('generar_aleatorios','lineaespera@generar_aleatorios');
Route::get('generar_aleatoriosx','montecarloController@generar_aleatorios');
Route::get('linea-espera-montecarlo','lineaespera@montecarlo');
Route::get('probabilidades_llegada','lineaespera@generar_tab_pro');
Route::get('linea-montecarlo-calculo','lineaespera@calcularlineamontecarlo');
Route::get('linea-montecarlo-calculox','montecarloController@calcularlineamontecarlo');
Route::resource('inventario-montecarlo','inventario');

Route::get('inventario_calculo','inventario@inventario');
Route::get('vista_demanda_prob','montecarloController@vista_demanda_prob');




Route::resource('montecarlo','montecarloController');

