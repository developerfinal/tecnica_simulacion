<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo_Usuario extends Model
{
    protected $table = 'ad_tipousuario';
 	protected $hidden = [];
}
