<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use View;
use Illuminate\Http\Request;
use Auth;
use Redirect;
use Session;
use DB;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */


    public function index()
    {
        return View::make('layouts.login');
    }
    public function store(Request $request)
    {

 // $dat=DB::table('users')::all();
  //dd($dat);
      // dd(User::all());

//dd($request);
        if(Auth::attempt(['cedula'=>$request['username'], 'password'=>$request['password']])){              
         return Redirect::to('inicio');      
        }
        else{
             Session::flash('message-error','Usuario No Existe');
                return Redirect::to('login');
            /*
             $cedulaconsult = UsuarioModel::where('login',$request->username)->first();
             if(!is_null($cedulaconsult))
             {  
            Session::flash('message-error','Nombre de usuario y/o contraseña incorrectos.');
            return Redirect::to('login');
            }else
            {
                   Session::flash('message-error','Usuario No Existe');
                return Redirect::to('login');
            }
            */
        } 
                    
    }
    public function logout()
    {
        Auth::logout();
        return Redirect::to('login');
    }
}
