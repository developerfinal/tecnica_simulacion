<?php

namespace App\Http\Controllers;
use mPDF;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Tipo_Usuario;
use View;
use Auth;
use Mail;
use Session;
use Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Mail\Mailable;
use App\Http\Requests\TipoCrearRequest; 
use App\Http\Requests\CambiarPasswordRQ;
use App\Http\Requests\ModificarUserRQ;
use Illuminate\Support\Facades\Input;


class TipoUsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
 

    public function index()
    {
         $tipos = Tipo_Usuario::all();
        // dd($tipos);
         return View::make('admin.tipo_usuarios.index', compact('tipos'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return View::make('admin.tipo_usuarios.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   

    public function store(Request  $request)
    {
        
         $this->validate($request, [
               
            'tipo' => 'required',  
            
            ] );  
        $tiposave = new Tipo_Usuario();
        $tiposave->tipo_usuario=$request->tipo;
        $tiposave->estado=$request->descripcion;
        $tiposave->save();
        Session::flash('message','Se ha ingresado un nuevo rol de Usuario al Sistema');
        return Redirect::to('tipousuarios');   
    }       

    /**
     * Display the specified resource.
     * 
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipo_usuario = Tipo_Usuario::find($id);
            return View::make('admin.tipo_Usuarios.editar', compact('tipo_usuario','id'));
    }        
       
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $tiposave =Tipo_Usuario::find($id);
        if($tiposave->tipo!=$request->tipo)
        {
          $this->validate($request, [
            'tipo' => 'required|unique:tipo_usuario,tipo,'.$tiposave->tipo,
            ]);            
        }         
        $tiposave->tipo=$request->tipo;
        $tiposave->descripcion=$request->descripcion;
        $tiposave->save();
        Session::flash('message','Se ha ingresado un nuevo rol de Usuario al Sistema');
        return Redirect::to('tipousuarios');   
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

         try 
        {         
            Tipo_Usuario::find($id)->delete();
            Session::flash('message','Se ha eliminado correctamente el tipo de usuario');
            
        } catch (\Illuminate\Database\QueryException $e) {
          Session::flash('message-error','Existen usuarios con este rol');
        }
        return Redirect::to('tipousuarios');   
    }
   

        /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    
}


    