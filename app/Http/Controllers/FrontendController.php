<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use View;
use Session;
use Response;
use Redirect;
use Hash;
use Illuminate\Support\Facades\DB;



use Illuminate\Support\Facades\Input;

class FrontendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
 

    public function index()
    {


    return view('admin.inicio'); 
    }
    public function seguro_eliminar($modelo,$id)
    {
      return View::make('portables.seguro_eliminar',compact('modelo','id'));
    }
    public function cambiar_pass_vista()
    {
      $usuario = Auth::user();
      return View::make('cuenta.cambiarpassword',compact('usuario'));
    }
    public function cambiar_pass_path(Request $request, $id)
    {

         $this->validate($request, [
            'password' => 'required',
            'password2' => 'required',
            'password3' => 'required|same:password2',               
            ],
            
            [
              'password3.same'=>'Confirme correctamente su nueva contraseña',
            ]
            
          );

        $usuario = User::find($id);
        if (Hash::check($request->password, $usuario->password))
            {
              
                     if ($request->password == $request->password2) 
                    {
                        Session::flash('message-error', 'Las contraseñas nueva Contraseña es igual a la Actual,Por lo Tanto no se Actualizo');   
                    return Redirect('cambiar_contrasena');   
                    }else{
                     $usuario = User::find($id);
                     $usuario->password=bcrypt($request->password2);
                     $usuario->save();
                     Session::flash('message','Ha Actualizado su Contraseña correctamente');
                       return Redirect('inicio');
                    }
                

            }else
            {
                Session::flash('message-error','Las Contraseña actual no es la correcta'); 
                return Redirect('cambiar_contrasena');
            }
    }
    public function actualizar_datos_vista()
    {
      $usuario = Auth::user();
      return View::make('cuenta.editarinfo',compact('usuario'));
    }
    public function actualizar_informacion_logeado(Request $request, $id)
    {    


        $usuario = User::find($id);     
         
            if (Hash::check($request->password, $usuario->password))
            {
            $usuario =User::find($id);
            $usuario->name = $request->nombres;            
            $usuario->email =  $request->email;           
            $usuario->telefono = $request->telefono;   
            $usuario->save();
            Session::flash('message','Se ha actualizado sus datos correctamente');
            return Redirect::to('inicio');  
            }else
            {
                 $usuario =User::find($id);
                 Session::flash('message-error','¡Error! Su contraseña ingresada no es correcta');
                 return  View::make('cuenta.editarinfo',compact('usuario'));  
            }
        
        
    }
  

 
//

}
