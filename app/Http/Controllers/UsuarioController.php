<?php

namespace App\Http\Controllers;
use mPDF;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Tipo_Usuario;
use App\mercadoModel;
use View;
use Auth;
use Hash;
use Response;
use Mail;
use Session;
use Redirect;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Mail\Mailable;
use App\Http\Requests\RegistrarUserRQ;
use App\Http\Requests\CambiarPasswordRQ;
use App\Http\Requests\ModificarUserRQ;
use Illuminate\Support\Facades\Input;
use App\areamedicaModel;
use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Exception\RequestException;
use App\PacienteModel;
use App\PacientedeudaModel;
use App\PacientedeudaerrorModel;
class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
 

    public function index()
    {
        

     
     // dd('kd');
        $usuarios = User::join('ad_tipousuario','ad_tipousuario.id','=','users.id_tipo_usuario')
        ->join('mercado','mercado.id','=','users.mercado_id')
        ->get(['users.id','cedula','name','email','telefono','fecha_nacimiento','tipo_usuario','mercado.descripcion as mercado_id']);

         return View::make('admin.usuarios.index', compact('usuarios'));
    }
  
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
           $tipos=Tipo_Usuario::pluck('tipo_usuario','id'); 
            $mercado=mercadoModel::pluck('descripcion','id'); 
             // dd($mercado);
     return View::make('admin.usuarios.crear',compact('tipos','mercado'));

             
        //return View::make('admin.usuarios.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   

    public function store(Request $request)
    {

         $this->validate($request, [
            'cedula' => 'required|unique:users',   
            'name' => 'required|unique:users',     
            'telefono' => 'required',  
            'email' => 'required|email',
               
            ]
            /*
            [
              'cedula.required'=>'Esta cedula es incorrecta',

            ]
            */
          );
        try {
        $usuario =new User();
        $usuario->name =  $request->name;
        $usuario->email = $request->email; 
        $usuario->cedula = $request->cedula; 
        $usuario->telefono = $request->telefono;     
        $usuario->fecha_nacimiento = date('Y-m-d', strtotime($request->fecha_nacimiento));    
        $usuario->id_tipo_usuario=$request->id_tipo_usuario;   
        $usuario->mercado_id = $request->mercado_id;
        $new_pass=$request->password; 

        if ($new_pass==null) 
        {
              $new_pass=bcrypt("1234");
        }else
        {
            $new_pass=bcrypt($new_pass);
        }

        $usuario->password=$new_pass;
        $usuario->save();
        Session::flash('message','Usuario añadido correctamente.');
        } catch (\Illuminate\Database\QueryException $e) {
        Session::flash('message-error','No se pudo añadir el usuario'.$e);
        }
        return Redirect::to('usuarios');
       
       
           
    }       

    /**
     * Display the specified resource.
     * 
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // EDITAR DESDE ADMIN
    public function edit($id)
    {
        $usuario = User::find($id);
        $tipos=Tipo_Usuario::pluck('tipo_usuario','id'); 
        $mercado=mercadoModel::pluck('descripcion','id'); 
        return View::make('admin.usuarios.editar', compact('usuario','tipos','mercado'));
    }
        //EDITAR DESDE USUARIO PERSONAL

            public function ActualizarInf()
    {
            $id = Auth::user()->id;
            $usuario = UsuarioModel::find($id);
            return View::make('usuario.EditarInfo', compact('usuario','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // ACTUALIZA LOS CAMBIOS A LA BD DESDE ADMIN
    public function update(Request $request, $id)
    {       

        try {
        $usuario =User::find($id);
        $usuario->name =  $request->name;
        $usuario->email = $request->email; 
        $usuario->cedula = $request->cedula; 
        $usuario->telefono = $request->telefono;     
        $usuario->fecha_nacimiento = date('Y-m-d', strtotime($request->fecha_nacimiento));    
        $usuario->id_tipo_usuario=$request->id_tipo_usuario;   
        $usuario->mercado_id = $request->mercado_id;
        $new_pass=$request->password; 

        if ($new_pass==null) 
        {
              $new_pass=bcrypt("1234");
        }else
        {
            $new_pass=bcrypt($new_pass);
        }

        $usuario->password=$new_pass;
        $usuario->save();
        Session::flash('message','Usuario añadido correctamente.');
        } catch (\Illuminate\Database\QueryException $e) {
        Session::flash('message-error','No se pudo añadir el usuario'.$e);
        }
        return Redirect::to('usuarios');
       
        
        
    }

      public function actualizar_foto(Request $request)
    {    
        $usuario = Auth::User();
        $fileName = $usuario->foto;             
            $dir = public_path().'/images/';
            $docs = $request->file('path');
              if (Input::hasFile('path'))
                {
                    $fileName = $docs->getClientOriginalName();
                    $existfile=false;
                     $consulusuario=UsuarioModel::where('foto',$fileName)->first();
                     if ($usuario->foto != null) {
                         $archidelete = $usuario->foto;
                        File::delete(public_path().'/images/'.$archidelete);
                     }
                     
                     if (is_null($consulusuario)) {        
                        $docs->move($dir, $fileName);
                        $usuario->foto = $fileName;
                     } else {
                            
                            $con=1;
                            do {     
                            $busc=$con.$fileName;   
                            $consulusuario=UsuarioModel::where('foto',$busc)->first();
                            if (is_null($consulusuario)) { 
                            $docs->move($dir, $busc);            
                            $usuario->foto=$busc;      
                            $existfile=false;        
                            }else
                            {
                                $existfile=true;
                            }    
                            $con=$con+1;
                            } while ($existfile==true);
                     }
                }           
            $usuario->save();
            Session::flash('message','Ha actualizado su foto correctamente');
            return Redirect::to('inicio');  
           
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function mostrar_popup_eliminar($id)
    {
       return  View::make('admin.usuarios.portables.eliminar',compact('id'));
    }
    public function destroy($id)
    {
      $userr=User::find($id);
     if ($userr->id_tipo_usuario!=3) {
            $userr->delete();
               Session::flash('message','Se ha eliminado correctamente al Usuario');
            }
            else
            {
              Session::flash('message-error','No se puede eliminar a un usuario administrador');
            } 
            return Redirect::to('usuarios'); 
    }
    
   
}
