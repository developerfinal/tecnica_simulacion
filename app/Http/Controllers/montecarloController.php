<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class montecarloController extends Controller
{
    public function index()
    {
        return view('inicio.montecarlo');
    }
    function vista_demanda_prob(Request $request)
    {
         $valor_n = $request->cantidad;
         $tipo=3;
        return view('desnudas.demanda_probabilidad_montecarlo',compact('valor_n','tipo'));
    }


    public function generar_aleatorios(Request $request)
    {
        $aleatorios_llegada = [];
        $eventos = $request->eventos;
        for ($i=0; $i < $eventos; $i++) { 
            $aleatorio = 1000/rand(1,1000);
            $randomx = round(($aleatorio - intval($aleatorio)),3);
            array_push($aleatorios_llegada,$randomx);
        }
     
        return view('desnudas.aleatorios_montecarlo',compact('aleatorios_llegada'));
    }


    public function calcular_linea_espera(Request $request)
    {
        $llegadas = $request->aleatorio_llegada;
        $servicios = $request->aleatorio_servicio;
        $contador = 0;
        $coun = 0;
        $lamda = $request->lamda;
        $niu = $request->niu;
        $datos = [];

        $fila[0] = $contador;
        $fila[1] = ' - ';
        $fila[2] = ' - ';
        $fila[3] = ' - ';
        $fila[4] = ' - ';
        $fila[5] = 0;
        $fila[6] = 0;
        $fila[7] = 0;
        $fila[8] = 0;
        $fila[9] = 0;
        array_push($datos,$fila);
        for ($i=0; $i < count($llegadas); $i++) {
            $contador = $contador + 1;
            $fila[0] = $contador;
            $llegada = $llegadas[$i];
            $fila[1] = $llegada;
            $servicio = $servicios[$i];
            $fila[2] = $servicio;
            $entre_llegadas = (-1/$lamda)*log($llegada);
            $fila[3] = round($entre_llegadas,2);
            $t_servicio = (-1/$niu)*log($servicios[$i]);
            $fila[4] = round($t_servicio,2);
            $llegada_exacta = round($datos[$i][5] + $entre_llegadas,2);
            $fila[5] = $llegada_exacta;
            $inicio_servicio = max($llegada_exacta,$datos[$i][7]);
            $fila[6] = round($inicio_servicio,2);
            $fin_servicio = round($inicio_servicio + $t_servicio,2);
            $fila[7] = $fin_servicio;
            $t_espera = round($inicio_servicio - $llegada_exacta,2);
            $fila[8] = $t_espera;
            $t_sistema = round($t_espera + $t_servicio,2);
            $fila[9] = $t_sistema;
            array_push($datos, $fila);
        }

        return view('desnudas.lineaesperatabla',compact('datos','contador'));
    }

    public function montecarlo()
    {
        return view('inicio.lineaesperamontecarlo');
    }

    public function generar_tab_pro(Request $request)
    {
        $valor_n = $request->cantidad;
        $tipo = $request->tipo;
        return view('desnudas.tabla_tab_pro',compact('valor_n','tipo'));
    }

        public function calcularlineamontecarlo(Request $request)
    {  
        
        $aleatorios_llegada=$request->aleatorio_llegada;
        $total=0;
        $tabla_pro_demanda = [];
        $demandas=$request->valor3;
        $probabilidades=$request->probabilidad3;
        $eventos=count($request->valor3);
        $acumulador_pro_dem = 0;
        $acumulador_rango_menor = 0;
        for ($i=0; $i < $eventos; $i++) { 
            $total=$total+$demandas[$i];
            $tabla_pro_demanda[$i][0] = $demandas[$i];
            $tabla_pro_demanda[$i][1] = $probabilidades[$i];
            $acumulador_pro_dem = $acumulador_pro_dem + $probabilidades[$i];
            /*
            if($i==2){
            dd($acumulador_pro_dem);
            }
            */
            $tabla_pro_demanda[$i][2] = round($acumulador_pro_dem,3);
            $tabla_pro_demanda[$i][3] = round($acumulador_rango_menor,3);
            $acumulador_rango_menor = round($acumulador_pro_dem,3) + 0.001;
            $tabla_pro_demanda[$i][4] = $acumulador_pro_dem;
        }

       /* $tabla_pro_entrega = [];
        $acumulador_pro_ent = 0;
        $acumulador_rango_menor = 0;
        for ($i=0; $i < count($request->valor4); $i++) { 
            $tabla_pro_entrega[$i][0] = $request->valor4[$i];
            $tabla_pro_entrega[$i][1] = $request->probabilidad4[$i];
            $acumulador_pro_ent = $acumulador_pro_ent + $request->probabilidad4[$i];
            $tabla_pro_entrega[$i][2] = $acumulador_pro_ent;
            $tabla_pro_entrega[$i][3] = round($acumulador_rango_menor,3);
            $acumulador_rango_menor = round($acumulador_pro_ent,3) + 0.001;
            $tabla_pro_entrega[$i][4] = $acumulador_pro_ent;
        }

        $tabla_calculada = [];
        $contador_res = 1;
        $hora_llegada_exacta = 0;
        $hora_fin_servicio = 0;
        $marcador_entrega = 0;
        for ($i=0; $i < count($request->aleatorio_llegada); $i++) { 
            $tabla_calculada[$i][0] = $contador_res;
            $tabla_calculada[$i][1] = $request->aleatorio_llegada[$i];
            $deandas_ventas = buscar_en_pro($tabla_pro_demanda, $request->aleatorio_llegada[$i]);
            $tabla_calculada[$i][2] = $deandas_ventas;
            $tabla_calculada[$i][3] = $inv_ini;
            $inv_tem = $inv_ini;
            $inv_ini = $tabla_calculada[$i][3] - $tabla_calculada[$i][2];
            if ($inv_ini < 0) {
                $inv_ini = 0;
            }
            $tabla_calculada[$i][4] = 0;
            if ($marcador_entrega != 0) {
                if ($tabla_calculada[$i][0] == $marcador_entrega) {
                    $tabla_calculada[$i][4] = $q;
                    $inv_ini = $inv_ini + $q - $tabla_calculada[$i][2] + $inv_tem;
                    $marcador_entrega = 0;
                }
            }

            $tabla_calculada[$i][5] = $inv_ini;
            $costo_faltante = 0;
            if ($tabla_calculada[$i][2] > ($tabla_calculada[$i][3] + $tabla_calculada[$i][4])) {
            $costo_faltante = $cf;
            }
            $tabla_calculada[$i][6] = $costo_faltante;

            $costo_mantener=0;
            $costo_mantener = $ch * $tabla_calculada[$i][5];
            $tabla_calculada[$i][7] = $costo_mantener;

            $orden = 0;
            if ($marcador_entrega == 0) {
                if ($tabla_calculada[$i][5] <= $r) {
                    $orden = $co;
                }
            }
            $tabla_calculada[$i][8] = $orden;

            $tabla_calculada[$i][9] = $request->aleatorio_servicio[$i];
            $t_entrega = 0;
            if ($marcador_entrega == 0) {
                if ($tabla_calculada[$i][5] <= $r) {
                    $t_entrega = buscar_en_pro($tabla_pro_entrega, $request->aleatorio_servicio[$i]);
                }
            }

            $tabla_calculada[$i][10] = $t_entrega;
            $dia_entrega = 0;
            if ($marcador_entrega == 0) {
                if ($tabla_calculada[$i][10] > 0) {
                    $dia_entrega = $tabla_calculada[$i][10] + $tabla_calculada[$i][0] + 1;
                }
            }
            $tabla_calculada[$i][11] = $dia_entrega;
            if ($tabla_calculada[$i][11] != 0) {
                $marcador_entrega = $tabla_calculada[$i][11];
            }
            $contador_res = $contador_res + 1;
        }
        */
     //  dd($tabla_pro_demanda);
        return view('desnudas.resultado_montecarlo',compact('tabla_pro_demanda','total','aleatorios_llegada'));
    }
    
}
