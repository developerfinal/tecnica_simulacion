function toastx(head, content, icon, time) {
    $.toast({
    heading:head,
    text:content,
    //position: 'top-right',
    position: 'bottom-right',
    stack: 3,
    icon: icon,
    hideAfter: time,
    showHideTransition: 'plain',
    allowToastClose: true,
    loader: false
    });
}